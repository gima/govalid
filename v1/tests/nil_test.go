package govalid_test

import (
	"testing"

	v "go.255.fi/govalid/v1"
)

func TestNil(t *testing.T) {
	test(t, "nil", true, v.Nil(), nil)
	test(t, "non-nil", false, v.Nil(), "")
}
